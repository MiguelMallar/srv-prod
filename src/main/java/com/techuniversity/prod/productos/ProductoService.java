package com.techuniversity.prod.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findByid(String id) {
        return productoRepository.findById(id);
    }

    public ProductoModel save(ProductoModel producto){
        return productoRepository.save(producto);
    }

    public boolean deleteProducto(ProductoModel producto){
        try {
            productoRepository.delete(producto);
            return true;
        } catch (Exception exception){
            return false;
        }

    }

}
