package com.techuniversity.prod.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ServiciosService {
    MongoCollection<Document> servicios;

    public static MongoCollection<Document> getServiciosCollection(){
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("backMayo");
        return database.getCollection("servicios");
    }

    public static List getAll(){
        MongoCollection<Document> servicios = getServiciosCollection();
        List list = new ArrayList();
        FindIterable<Document> iterDoc = servicios.find();
        Iterator iterador = iterDoc.iterator();
        while (iterador.hasNext()){
            list.add(iterador.next());
        }
        return list;
    }

    public static void insert(String cadenaServicio) throws Exception{
        Document doc = Document.parse(cadenaServicio);
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertOne(doc);
    }

    public static void insertBatch(String cadenaServicio) throws Exception{
        Document doc = Document.parse(cadenaServicio);
        List<Document> lstServicios = doc.getList("servicios", Document.class);
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertMany(lstServicios);
    }

    public static List<Document> getFiltrados(String cadenaFiltro){
        MongoCollection<Document> servicios = getServiciosCollection();
        List list = new ArrayList();
        Document docFiltro = Document.parse(cadenaFiltro);
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        Iterator iterador = iterDoc.iterator();
        while (iterador.hasNext()){
            list.add(iterador.next());
        }
        return list;
    }

    public static List<Document> getPeriodosFiltrados(Document docFiltro){
        MongoCollection<Document> servicios = getServiciosCollection();
        List list = new ArrayList();
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        Iterator iterador = iterDoc.iterator();
        while (iterador.hasNext()){
            list.add(iterador.next());
        }
        return list;
    }

    public static void update(String filtro, String valores){
        MongoCollection<Document> servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document docValor = Document.parse(valores);
        servicios.updateOne(docFiltro, docValor);
    }
}
