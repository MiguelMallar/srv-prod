package com.techuniversity.prod.controllers;

import com.techuniversity.prod.productos.ProductoModel;
import com.techuniversity.prod.productos.ProductoService;
import com.techuniversity.prod.servicios.ServiciosService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/productos")
public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos(){
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id) {
        return productoService.findByid(id);
    }

    @PostMapping("/productos")
    public ProductoModel insertProductoModel(@RequestBody ProductoModel nuevo) {
        productoService.save(nuevo);
        return nuevo;
    }

    @PutMapping("/productos")
    public void updateProductoModel(@RequestBody ProductoModel producto) {
        productoService.save(producto);

    }
    @DeleteMapping("/productos")
    public boolean deleteProductoModel(@RequestBody ProductoModel producto) {
        return productoService.deleteProducto(producto);
    }


}
